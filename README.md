# API Anulación licencias tipo pago por adelantado.

Anula las licencias de tipo pago adelantado tomando el siguiente procedimiento cumpliendo uno o mas de los mismos dependiendo del caso. Se valida es estado de fum, la fecha de vencimiento del mismo y si l actividad econónica cuenta con una superficie; se procede a anular la licecia, se genera una nueva se gurdan los datos en sierra, se procede a realizar la notificación al ciudadano mediante igob, se registran los datos necesarios en cero papel.<br>
Si desea conocer un poco el detalle de los servicios empleados por el API puede seguir el siguiente [enlace].

### Tabla de contenido
- Instalación
- Uso
- Creditos
- Licencia

## Instalación
Para realizar la instalación del API leer el siguiente [manual].

## Uso

Una ves instalado el API en directorio correspondiente se debe programar el mismo en el archivo cron desde el usuario que administrara el API.

El API puede ser utilizado de las siguientes formas:

**1) Programación del API en el archivo crontab de un sistema linux (debian 10).**

```sh
1 0 * * * /usr/bin/node ~/licencias-demon
```


El API se programó para ejecutarse todos los dias en el minuto **1** de la hora **0**.

Es necesario aclarar que la ruta anterior (**/usr/bin/node**) es la dirección donde nodejs esta instalado y puede variar de acuerdo a la configuración del sistema anfitrión.

**2) Ejecución manual en un ambiente de producción.**
```sh
$ node index.js
```
Se debe ingrasar al directorio raíz del proyecto antes de ejecutar el comando.

**3) Ejecución en modo desarrollo.**

```sh
$ npm run dev
```

Se debe ingrasar al directorio raíz del proyecto antes de ejecutar el comando e instalar las dependencias de desarrollo del proyecto.

## Creditos

Gobierno autónomo municipal de La Paz - DGEM/UDIT.

Encargado del proyeto v1:
- Jorge Alanoca

Encargado del proyeto v2:
- MARCO ALTAMIRANO

Colaboradores v1:
- Estela Ramos
- Janeth Martinez
- Patricia Condori



## Licencia
MIT

[manual]: <docs/INSTALL.md>
[enlace]: <docs/SERVICIOS.md>
