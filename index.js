'use strict';

let anulaLicencia = (() => {
    var ref = _asyncToGenerator(function* () {
        console.log("Demonio inicializado.");
        try {
            yield anularLicencia(function (res) {
                console.log(res);
            });
        } catch (error) {
            console.error("Error al intentar anular las licencias:", error);
        }
    });

    return function anulaLicencia() {
        return ref.apply(this, arguments);
    };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

var _require = require('./app/controllers/licenciapga.controller');

var anularLicencia = _require.anularLicencia;

anulaLicencia();
/*

Obtiene datos de un trámite
**192.168.5.141:9091/wsIf/lstTramitesPagoAdelantado
  {
    idActividad: id_ae,
    caso: hoja_ruta
  }

Notificación al ciudadano
**192.168.5.141:8003/wsRCPG/crearnotificacion
  {
    tramite: tramite,
    observacion: "NOTIFICACION - LICENCIA PAGO ADELANTADO",
    usuarioid: "1",
    actividad: "SOBRE PAGO ADELANTADO",
    sistema: "CERO PAPEL - LICENCIAS",
    usuario: "Notificación anulación de licencia de funcionamiento - Sierra."
  }

Guarda url licencia en igob
**192.168.5.141:8001/wsRCPG/gDocumentosIf
  {
    sdoc_sistema: "SIERRA",
    sdoc_proceso: "EMISION DE LICENCIA DE TIPO PAGO POR ADELANTADO",
    sdoc_id: 1,
    sdoc_ci_nodo: "DMS",
    sdoc_datos: "",
    sdoc_url: urldatabase,
    sdoc_version: 1,
    sdoc_tiempo: 730,
    sdoc_firma_digital: "0",
    sdoc_usuario: id_ciudadano,
    // CI_BIGDATA = 57798c6f2f59181eb27baa3c
    sdoc_tipo_documento: "pdf",
    sdoc_tamanio_documento: "",
    sdoc_nombre: "LICENCIA DE FUNCIONAMIENTO",
    sdoc_tps_doc_id: 0,
    sdoc_url_logica: urldatabase,
    sdoc_acceso: "",
    sdoc_tipo_documento_ext: "",
    sdoc_nrotramite_nexo: "",
    sdoc_id_codigo: tramite // AE_NRO_CASO
  }

Guarda url de licencia en CERO PAPEL
**192.168.5.141:9091/wsIf/crearAdjunto
  {
    doc_sistema: "IF_AE_REGULAR",
    doc_proceso: "EMISION REGULAR DE LICENCIAS DE FUNCIONAMIENTO",
    doc_id: casoid,
    // casoid
    doc_ci_nodo: nodo,
    // nodonombre
    doc_datos: "",
    doc_titulo: "",
    doc_palabras: "",
    doc_url: url_licencia,
    // 1) con superficie => nueva licencia 2) sin superficie => urllicencia
    doc_version: 1,
    doc_tiempo: 60,
    doc_firma_digital: "0",
    doc_estado: "",
    doc_usuario: "",
    doc_tipo_documento: "pdf",
    doc_tamanio_documento: "",
    doc_nombre: url_licencia,
    doc_tps_doc_id: 0,
    doc_url_logica: url_licencia,
    doc_acceso: "",
    doc_cuerpo: "",
    doc_tipo_documentacion: "",
    doc_tipo_ingreso: "",
    doc_estado_de_envio: "",
    doc_correlativo: "",
    doc_tipo_documento_ext: "",
    doc_nrotramite_nexo: "",
    doc_id_codigo: ae_nro_caso,
    // AE_NRO_CASO
    doc_id_carpeta: 0
  }

Actualiza datos de la AE en CERO PAPEL
**192.168.5.141:9091/wsIf/guardarFormCasos
  {
    datos: datosSerializados,
    fmodificado: "".concat(md.getFullYear(), "-").concat(md.getMonth() + 1, "-").concat(md.getDate()),
    usrid: "677",
    casid: full_data_lic.data.success.data[0].casoid
  }*/