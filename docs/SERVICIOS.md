## Lista de servicios utilizados por el API

### Servicios

Los servicios consumidos por el API se listan a continuación:

|Nro|Entorno| Servicio | Dscripción |
| ------ | ------ | ------ | ------ |
| 1 |SIERRA| /api/apiLogin | Se obtiene un token autorizado del sistema sierra. |
| 2 | IGOB | /wsIf/lstTramitesPagoAdelantado | Obtiene la actividad económica de tipo pago adelantado desde cero papel. |
| 3 | IGOB | /wsRCPG/crearnotificacion | Crea una nueva notificación en la cuenta igob de un ciudadano, infomando la anulación de su licencia. |
| 4 | IGOB | /wsIf/crearAdjunto | ------ |
| 5 | IGOB | /wsIf/guardarFormCasos | Actualiza los datos de la actividad aconómica en cero papel. |
| 6 | SIERRA | SERVICIO_SIERRA-MAE-3411 | Lista las actividades económicas con sus respectivos fums tipo pago adelantado desde sierra. |
| 7 | VALLE | SERVICIO_VALLE-LFUN-3415 | Obtiene los datos de una actividad económica desde cero papel. |
| 8 | VALLE | SERVICIO_VALLE-LFUN-3373 | Anula la licencia de una actividad económica en el sistema sierra. |
| 9 | VALLE | /api/licensesPago | Genera una nueva licencia de funcionamiento de una actividad económica en el sistema sierra. |
| 10 | VALLE | VALLE_PRUEBA-SGEN-3406 | Guarda la nueva licencia generada en el sistem sierra. |
| 11 | VALLE | SERVICIO_VALLE-LFUN-3420 | Guarda las actividades realizadas por el API en el sistema sierra. |

Regresar [README].

[README]: <../README.md>