## Guía de instalación en un entorno de producción

### Requerimientos del sistema

- **Ubuntu 20.x**
- **NodeJS** v10.19.0
- **npm** v6.14.6

### Clonar el proyecto
(Token disponible solo para la genstión 2020)
```sh
$ cd ~/
$ git clone --branch gamlp https://guest:XCp-mtu_psyQnJjNF│y5R@gitlab.com/George543/licencias-demon
```

Instalación de dependencias

```sh
$ npm install --production
```

### Configuración del API:
#### **Configuración del archivo de autenticación**

```sh
$ cd licencias-demon
$ cp config/auth-config.js.example config/auth-config.js
```
En el archivo **auth-config.js** debe configurarse con las credenciales necesarias para adquirir un token autorizado en el motor de servicios.
#### **Configuración del archivo para el consumo de servicios**

```sh
$ cd licencias-demon
$ cp config/server-point.js.example config/server-point.js
```
En el archivo **server-point.js** debe configurarse los servidores correspondientes a los servicios requeridos.

#### **Configuración del archivo de registro de actividades**
```sh
$ cd licencias-demon
$ cp app/logs/registro.json.example app/logs/registro.json
```
En el archivo **registro.json** almacena la actividad realizada por el demonio en un formato json en un maximo de 100 procesos de anulación ejecutadas por el demonio.

Regresar [README].

[README]: <../README.md>
