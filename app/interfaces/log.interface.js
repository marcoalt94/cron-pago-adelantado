function Log (etapa_id = "", etapa = "", proceso = "", estado = "", inicio = new Date(), fin = "", param_in = "", param_out = "", error = "") {
    this.etapa_id = etapa_id;
    this.etapa = etapa;
    this.proceso = proceso;
    this.estado = estado;
    this.inicio = inicio;
    this.fin = fin;
    this.param_in = param_in;
    this.param_out = param_out;
    this.error = error;
}

module.exports = {
    Log
}