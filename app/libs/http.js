'use strict'

var http = require('http');

function getRequest (options) {
    let op = getOpcions ('get', options, null);
    return new Promise((resolve, reject) => {
        var req = http.request(op, (res) => {
            var str = '';

            // if (res.statusCode < 200 || res.statusCode >= 300) {
            //     return reject(new Error('statusCode=' + res.statusCode));
            // }

            res.on('data', function (chunk) {
                str += chunk;
            });

            res.on('end', function () {
                let res = "";
                try {
                    res = JSON.parse (str);
                    if (res == '[{}]') reject ("Ningún dato encontrado");
                    else resolve (res);
                } catch(err) {
                    console.error (err);
                    reject(err);
                }
                
            });
        });
        req.on('error', error => {
            reject (error);
        })
        req.end();
    });
}

function postRequest (runner_name, options, data) {
    let op = getOpcions ('post', options, data);
    return new Promise((resolve, reject) => {
        var req = http.request(op, (res) => {
            var str = '';
            res.on('data', function (chunk) {
                str += chunk;
                /* if (runner_name == 'valle.guardarLog') {
                    console.log ("CHUNK!!!!!!");
                    console.log (str);
                    console.log (JSON.parse (str));
                } */
            });
            res.on('end', function () {
                let res = "";
                try {
                    res = JSON.parse (str);
                    if (res.error) {
                        if (res.error != "0"){
                            throw new Error (JSON.stringify({
                                data: res,
                                estado: false,
                                mensaje: "Error an intentar ejecutar la solicitud: " + runner_name,
                                flag: "warning"
                            })).stack;
                        } else {
                            resolve ({
                                data: res,
                                estado: true,
                                mensaje: "Datos obtenidos correctamente.",
                                flag: "success"
                            });
                        }
                        
                    } else if (res == '[{}]') {
                        resolve ({
                            data: {},
                            estado: false,
                            mensaje: "Ningún dato encontrado.",
                            flag: "warning"
                        });
                    } else {
                        resolve ({
                            data: res,
                            estado: true,
                            mensaje: "Datos obtenidos correctamente.",
                            flag: "success"
                        });
                    }
                } catch (err) {
                    reject(err);
                }
            });
        });
        req.on('error', error => {
            console.error ("ERROR postRequest: " + runner_name, error);
            reject (error);
        })
        req.write(data);
        req.end();
    });
}

function getOpcions (method, options, data) {
    var op = {
        host: options.host,
        port: options.port,
        path: options.path,
        method: "",
        headers: {}
    };

    if (method === 'post') {
        op.method = "POST";

        if (!options.headers) op.headers = {
            'Content-Type': 'application/json; charset=utf-8',
            'Content-Length': data? Buffer.byteLength (data) : Buffer.byteLength ("")
        }; else {
            op.headers = options.headers;
        }

        return op;

    } else {
        op.method = "GET";
    }
    
    return op;
}

module.exports = {
    postRequest,
    getRequest
}