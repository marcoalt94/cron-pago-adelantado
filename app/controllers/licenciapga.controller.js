"use strict";

let anularLicencia = (() => {
  var ref = _asyncToGenerator(function* (callback) {
    var log = new Log(1, "Inicialización demonio.", "Inicialización", "iniciado", new Date(), undefined, "", "", false);
    var token = null;

    try {
      token = yield runGetTokenMotor();
      token = token.data.token; // TODO: no se esta notificando a los fums pagados, se debe hacerlo.
      var fm = yield runListaFums(token);
      if (fm.estado) fm = fm.data;else {
        throw new Error(fm.mensaje);
      }
      console.log('VENCIDOS..',fm);
      log.estado = "finalizado";
      for (var i = 0; i < fm.length; i++) {
        try {
          aelog = new Log(fm[i].actividad_id, "Proc. anulación de licencia, odm.", "Anulación", "iniciado", new Date(), undefined, "", [], "ninguno");
          aelog.estado = "finalizados";
          var ae = yield runAePagoAdelantado(token, fm[i].actividad_id);
          if (ae.estado){
            ae = ae.data;
          }else {
            throw new Error(ae.mensaje);
          }
          console.log('ae------',fm[i].actividad_id,ae);
          if (ae.superficie > 0) {
            yield _asyncToGenerator(function* () {
              var fms = (fm[i].nro_odm);
              var licencia_anulada = yield runAnulaLicencia(token, fms,ae.actividad_id,ae.superficie);
              console.log('licencia_anulada',licencia_anulada);
              var respuesta = function respuesta() {
                try {
                  return licencia_anulada;
                } catch (e) {
                  return null;
                }
              };
              //console.log('licencia_anulada//',licencia_anulada);
              if (!respuesta()) {
                throw new Error(JSON.stringify(licencia_anulada));
              } else if (respuesta().data.proceso == true) {
                if(respuesta().data.cuatro_anios){
                    var tf = getSumarDias(ae.fecha_inicio, 730);
                    console.log('tf____',tf);
                    var _gl = yield runGenerarLicencia( null, ae, tf[1] );
                    console.log('------->>>>',_gl);
                    if (_gl.estado) _gl = _gl.data;else {
                      throw new Error(_gl.mensaje);
                    } // TODO: tipo_licencia_descripcion? => licencia_funcionamiento
                    var xnumero_lic = _gl.licencia_funcionamiento_id;
                    var xtipo_proc = 'emision';
                    var gul = yield runGuardarLicencia(token, ae, tf[0], _gl.url_interno,xnumero_lic,xtipo_proc);
                    //console.log('guardar',gul);
                    /*var ae_cero_papel = yield runGetAeDatos(null, fm[i].actividad_id, ae.hoja_ruta);

                    var id_tramite_igob = ae_cero_papel.data.success.data[0].datos.f01_nro_frm;
                    var id_tramite = ae_cero_papel.data.success.data[0].datos.AE_NRO_CASO;
                    var id_ciudadano = ae_cero_papel.data.success.data[0].datos.CI_BIGDATA;

                    var igob_notificacion = yield runNotificacionIgob(null, id_tramite_igob, id_ciudadano);
                    var igob_documento = yield runGuardarDocIgob(null, id_tramite, id_ciudadano, _gl.urldatabase);
                    var casoid = ae_cero_papel.data.success.data[0].casoid;
                    var nodo = ae_cero_papel.data.success.data[0].nodonombre;
                    var ae_nro_caso = ae_cero_papel.data.success.data[0].datos.AE_NRO_CASO;

                    var guardar_licencia_ae = yield runGuardarLicenciaAE(null, casoid, nodo, _gl.urldatabase, ae_nro_caso);
                    var guardar_caso_cp = yield runGuardarFormCasos(null, ae_cero_papel);
                    */
                }
              } else {
                throw new Error(JSON.stringify(licencia_anulada));
              }
            })();

          } else {   ///SIN SUPERFICIE
            yield _asyncToGenerator(function* () {
              var fms = (fm[i].nro_odm);
              //var licencia_anulada = yield runAnulaLicencia(token, fms);
              console.log('fms,ae.actividad_id,ae.superficie',fms,ae.actividad_id,ae.superficie);
              var licencia_anulada = yield runAnulaLicencia(token, fms,ae.actividad_id,ae.superficie);
              console.log('licencia_anulada',licencia_anulada);
              var respuesta = function respuesta() {
                try {
                  return licencia_anulada;
                } catch (e) {
                  return null;
                }
              };
              if (!respuesta()) {
                throw new Error(JSON.stringify(licencia_anulada));
              } else if (respuesta().data.proceso == true) {
                console.log('éxito');
                /*var ae_cero_papel = yield runGetAeDatos(null, fm[i].actividad_id, ae[0].hoja_ruta);
                var urllicencia = ae_cero_papel.data.success.data[0].datos.urllicencia;
                var id_tramite_igob = ae_cero_papel.data.success.data[0].datos.f01_nro_frm;
                var id_tramite = ae_cero_papel.data.success.data[0].datos.AE_NRO_CASO;
                var id_ciudadano = ae_cero_papel.data.success.data[0].datos.CI_BIGDATA;

                var igob_notificacion = yield runNotificacionIgob(null, id_tramite_igob, id_ciudadano);
                var igob_documento = yield runGuardarDocIgob(null, id_tramite, id_ciudadano, urllicencia);

                var casoid = ae_cero_papel.data.success.data[0].casoid;
                var nodo = ae_cero_papel.data.success.data[0].nodonombre;
                var ae_nro_caso = ae_cero_papel.data.success.data[0].datos.AE_NRO_CASO;
                
                var guardar_licencia_ae = yield runGuardarLicenciaAE(null, casoid, nodo, gl.urldatabase, ae_nro_caso);
                var guardar_caso_cp = yield runGuardarFormCasos(null, ae_cero_papel);
                */
              } else {
                throw new Error(JSON.stringify(licencia_anulada));
              }
            })();
          }
        } catch (error) {
          //console.log('error--------------',error.toString());
          log.estado = "fallido";
          log.error = true;
          aelog.estado = "fallido";
          aelog.error = error.toString();
        }

        aelog.fin = new Date();
        backlog.push(aelog);
      }

      log.fin = new Date();
      yield guardarLogs(token, log, backlog);
      callback("Proceso de anulación de licencias finalizada.");
    } catch (error) {
      log.estado = "fallido";
      log.error = error.toString();
      log.param_in = "";
      log.fin = new Date();
      yield guardarLogs(token, log, backlog);
      throw new Error(error);
    }
  });

  return function anularLicencia(_x) {
    return ref.apply(this, arguments);
  };
})();

let guardarLogs = (() => {
  var ref = _asyncToGenerator(function* (token, log, stacklog) {
    try {
      if (!Array.isArray(logs)) logs = [];
      if (logs.length > 1000) logs.shift();
      var newLog = {
        log: log,
        proceso: stacklog,
        origen: os.hostname()
      };
      var ebdlog = new Log(12, "Registro log BD", "Almacena log proceso en BD.", "inciado", new Date(), undefined, "ninguno", "ninguno", "ninguno");

      try {
        var res = yield guardarLog(token, newLog);

        if (res.data[0].sp_insertar_excepciones_proceso_revertir_anular_lf) {
          ebdlog.estado = "finalizado";
          ebdlog.param_out = res.data[0];
          ebdlog.fin = new Date();
        } else {
          throw new Error("No se logro registrar el log en la base de datos.");
        }
      } catch (error) {
        ebdlog.estado = "fallido";
        ebdlog.error = error.toString();
        ebdlog.fin = new Date();
      }

      newLog.registrodb = ebdlog;
      logs.push(newLog);
      writeDataToFile(__dirname.split("app")[0] + "app/logs/registro.json", logs);
    } catch (error) {
      console.error("Error fatal al guardar el log: ", error);
      throw new Error(error).stack;
    }
  });

  return function guardarLogs(_x2, _x3, _x4) {
    return ref.apply(this, arguments);
  };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

var querystring = require("querystring");

var os = require("os");

var _require = require("../libs/utils"),
    writeDataToFile = _require.writeDataToFile;

var logs = require("../logs/registro");

var _require2 = require("../models/igob.model"),
    getAeDatos = _require2.getAeDatos,
    igobNotificacion = _require2.igobNotificacion,
    igobGuardarLic = _require2.igobGuardarLic,
    guardarLicenciaAe = _require2.guardarLicenciaAe,
    guardarFormCasos = _require2.guardarFormCasos;

var _require3 = require("../models/sierra.model"),
    listarFumPagoAdelantado = _require3.listarFumPagoAdelantado;

var _require4 = require("../models/valle.model"),
    obtenerAePagoAdelantado = _require4.obtenerAePagoAdelantado,
    anulaLicencia = _require4.anulaLicencia,
    generarLicencia = _require4.generarLicencia,
    guardarLicencia = _require4.guardarLicencia,
    guardarLog = _require4.guardarLog;

var _require5 = require("../models/auth.model"),
    getTokenMotor = _require5.getTokenMotor,
    getUsuario = _require5.getUsuario;

var _require6 = require("../interfaces/log.interface"),
    Log = _require6.Log;

var backlog = [];
var aelog = {};

function runGetTokenMotor() {
  var log = new Log(1, "Inicialización demonio.", "Obtener token de autorización", "inciado", new Date(), undefined, "ninguno", "ninguno", "ninguno");

  try {
    return getTokenMotor().then(function (res) {
      log.estado = "finalizado";
      log.fin = new Date();
      backlog.push(log);
      return res;
    }).catch(function (err) {
      log.estado = "fallido";
      log.error = err;
      log.fin = new Date();
      backlog.push(log);
      throw new Error(err).stack;
    });
  } catch (error) {
    log.estado = "fallido";
    log.error = error;
    log.fin = new Date();
    backlog.push(log);
    throw new Error(error).stack;
  }
}

function runListaFums(token) {
  var log = new Log(2, "Recuperación de infomación.", "Obtener datos de ODM vencidos sierra.", "inciado", new Date(), undefined, "ninguno", [], "ninguno");
  return listarFumPagoAdelantado(token).then(function (res) {
    log.estado = "finalizado";
    log.fin = new Date();
    log.param_out = res;
    backlog.push(log);
    return res;
  }).catch(function (err) {
    log.estado = "fallido";
    log.error = err.toString();
    log.fin = new Date();
    backlog.push(log);
    throw new Error(err).stack;
  });
}

function runAePagoAdelantado(token, actividad_id) {
  var log = new Log(3, "Recuperación de infomación AE.", "Obtener datos actividad desde valle.", "inciado", new Date(), undefined, "ninguno", "ninguno", "ninguno");
  return obtenerAePagoAdelantado(token, actividad_id).then(function (res) {
    log.estado = "finalizado";
    log.fin = new Date();
    log.param_in = {
      actividad_id: actividad_id
    };
    log.param_out = res.data;//[0];
    aelog.param_out.push(log);
    return res;
  }).catch(function (err) {
    log.estado = "fallido";
    log.error = err.toString();
    log.param_in = {
      actividad_id: actividad_id
    };
    log.fin = new Date();
    aelog.param_out.push(log);
    throw new Error(err).stack;
  });
}

function runAnulaLicencia(token, fums,actividad_id,superficie) {
  var log = new Log(4, "Anulación de licencia..", "Anula la licencia.", "iniciado", new Date(), undefined, "ninguno", "ninguno", "ninguno");
  console.log('sssssssss','sssssssss');
  return anulaLicencia(token, fums,actividad_id,superficie).then(function (res) {
    log.estado = "finalizado";
    log.fin = new Date();
    log.param_in = {
      fums: fums
    };
    log.param_out = res.data;//.data[0];marco
    aelog.param_out.push(log);
    return res;
  }).catch(function (err) {
    log.estado = "fallido";
    log.error = err.toString();
    log.param_in = {
      fums: fums
    };
    log.fin = new Date();
    aelog.param_out.push(log);
    throw new Error(err).stack;
  });
}
function formatoFecha(string) {
  let conv = string.split('-').reverse().join('/');
  return conv;
}

function runGenerarLicencia(token, ae, tiempo_fin)  {
  var log =  new Log(5, "Generar nueva licencia.", "Genera una nueva licencia mediante un servicio de valle.", "inciado", new Date(), undefined, "ninguno", "ninguno", "ninguno");
  try {
    var form_data = {
      caso_id: ae.hoja_ruta,
      zona_id: ae.zona_segura == "false" ? 0 : ae.zona_id,
      actividad_economica_id: ae.actividad_id,
      fecha_inicio: formatoFecha(ae.fecha_inicio), //getSumarDias(ae.fecha_inicio, 1)[1], //TODO 
      fecha_fin: tiempo_fin,
      usuario: 'administrador', //TODO
      caso_oid: '111'   //TODO
    };
    return generarLicencia(token, form_data).then(function (res) {
      log.estado = "finalizado";
      log.fin = new Date();
      log.param_in = form_data;
      log.param_out = res.data;
      aelog.param_out.push(log);
      return res;
    }).catch(function (err) {
      log.estado = "fallido";
      log.error = err.toString();
      log.param_in = form_data;
      log.fin = new Date();
      aelog.param_out.push(log);
      throw new Error(err).stack;
    });
  } catch (error) {
    log.estado = "fallido";
    log.error = error.toString();
    log.param_in = {
      ae: ae,
      tiempo_fin: tiempo_fin
    };
    log.fin = new Date();
    aelog.param_out.push(log);
    throw new Error(error).stack;
  }
}

function runGuardarLicencia(token, ae, tiempo_fin, url_licencia,xnumero_lic,xtipo_proc) {
  var log = new Log(6, "Guardar licencia generada.", "Almacena la nueva licencia mediante un servicio de valle.", "inciado", new Date(), undefined, "ninguno", "ninguno", "ninguno");

  try {
    var datos = {
      numero: xnumero_lic,
      fecha_inicio: ae.fecha_inicio,
      fecha_fin: tiempo_fin,
      tipo_proceso: xtipo_proc,
      //tipo_licencia_descripcion: ae.tipo_licencia,
      ae_id: ae.actividad_id,
      url: url_licencia,
      data_usuario: {"usr_id":"1","usr_ip":"127.0.0.1","usuario":"cron","funcionario":"cron","equipo":"cron"},
      //usuario: getUsuario(),
      hoja_ruta: ae.hoja_ruta
    };
    return guardarLicencia(token, datos).then(function (res) {
      console.log('res/////////////',res);
      log.estado = "finalizado";
      log.fin = new Date();
      log.param_in = datos;
      log.param_out = res.data;//[0];
      aelog.param_out.push(log);
      return res;
    }).catch(function (err) {
      log.estado = "fallido";
      log.error = err.toString();
      log.param_in = datos;
      log.fin = new Date();
      aelog.param_out.push(log);
      throw new Error(err).stack;
    });
  } catch (error) {
    log.estado = "fallido";
    log.error = error.toString();
    log.param_in = {
      ae: ae,
      tiempo_fin: tiempo_fin,
      url_licencia: url_licencia,
      usuario: getUsuario()
    };
    log.fin = new Date();
    aelog.param_out.push(log);
    throw new Error(error).stack;
  }
}

function runGetAeDatos(token, id_ae, hoja_ruta) {
  var log = new Log(7, "Obtener datos de la actividad economica.", "Obtiene datos de la actividad econimica desde cero papel.", "inciado", new Date(), undefined, "ninguno", "ninguno", "ninguno");

  try {
    var params = {
      idActividad: id_ae,
      caso: hoja_ruta
    };
    return getAeDatos(token, params).then(function (res) {
      log.estado = "finalizado";
      log.fin = new Date();
      log.param_in = params;
      log.param_out = "Datos desde cero papel.";
      aelog.param_out.push(log);
      return res;
    }).catch(function (err) {
      log.estado = "fallido";
      log.error = err.toString();
      log.param_in = params;
      log.fin = new Date();
      aelog.param_out.push(log);
      throw new Error(err).stack;
    });
  } catch (error) {
    log.estado = "fallido";
    log.error = error.toString();
    log.param_in = {
      id_ae: id_ae,
      hoja_ruta: hoja_ruta
    };
    log.fin = new Date();
    aelog.param_out.push(log);
    throw new Error(error).stack;
  }
}

function runNotificacionIgob(token, tramite, id_ciudadano) {
  var log = new Log(8, "Notificación igob.", "Notificación al ciudadano.", "iniciado", new Date(), undefined, "ninguno", "ninguno", "ninguno");
  try {
    var notificacion = {
      tramite: tramite,
      // f01_nro_frm
      observacion: "NOTIFICACION - LICENCIA PAGO ADELANTADO",
      usuarioid: "1",
      // TODO: Obtener el ID usuario.
      actividad: "SOBRE PAGO ADELANTADO",
      sistema: "CERO PAPEL - LICENCIAS",
      usuario: "Notificación anulación de licencia de funcionamiento - Sierra."
    };
    return igobNotificacion(token, notificacion).then(function (res) {
      log.estado = "finalizado";
      log.fin = new Date();
      log.param_in = notificacion;
      log.param_out = res.data;
      aelog.param_out.push(log);
      return res;
    }).catch(function (err) {
      log.estado = "fallido";
      log.error = err.toString();
      log.param_in = notificacion;
      log.fin = new Date();
      aelog.param_out.push(log);
      throw new Error(err).stack;
    });
  } catch (error) {
    log.estado = "fallido";
    log.error = error.toString();
    log.param_in = {
      tramite: tramite,
      id_ciudadano: id_ciudadano
    };
    log.fin = new Date();
    aelog.param_out.push(log);
    throw new Error(error).stack;
  }
}

function runGuardarDocIgob(token, tramite, id_ciudadano, urldatabase) {
  var log = new Log(9, "Guardar documento igob.", "Guarda documento en igob.", "iniciado", new Date(), undefined, "ninguno", "ninguno", "ninguno");

  try {
    var datos_licencia = {
      sdoc_sistema: "SIERRA",
      sdoc_proceso: "EMISION DE LICENCIA DE TIPO PAGO POR ADELANTADO",
      sdoc_id: 1,
      sdoc_ci_nodo: "DMS",
      sdoc_datos: "",
      sdoc_url: urldatabase,
      sdoc_version: 1,
      sdoc_tiempo: 730,
      sdoc_firma_digital: "0",
      sdoc_usuario: id_ciudadano,
      // CI_BIGDATA = 57798c6f2f59181eb27baa3c
      sdoc_tipo_documento: "pdf",
      sdoc_tamanio_documento: "",
      sdoc_nombre: "LICENCIA DE FUNCIONAMIENTO",
      sdoc_tps_doc_id: 0,
      sdoc_url_logica: urldatabase,
      sdoc_acceso: "",
      sdoc_tipo_documento_ext: "",
      sdoc_nrotramite_nexo: "",
      sdoc_id_codigo: tramite // AE_NRO_CASO
    };
    return igobGuardarLic(token, datos_licencia).then(function (res) {
      log.estado = "finalizado";
      log.fin = new Date();
      log.param_in = datos_licencia;
      log.param_out = res.data;
      aelog.param_out.push(log);
      return res;
    }).catch(function (err) {
      log.estado = "fallido";
      log.error = err.toString();
      log.param_in = datos_licencia;
      log.fin = new Date();
      aelog.param_out.push(log);
      throw new Error(err).stack;
    });
  } catch (error) {
    log.estado = "fallido";
    log.error = error.toString();
    log.param_in = {
      tramite: tramite,
      id_ciudadano: id_ciudadano
    };
    log.fin = new Date();
    aelog.param_out.push(log);
    throw new Error(error).stack;
  }
}

function runGuardarLicenciaAE(token, casoid, nodo, url_licencia, ae_nro_caso) {
  var log = new Log(10, "Guardar la licencia generada.", "Se guarda la licencia en cero papel.", "inciado", new Date(), undefined, "ninguno", "ninguno", "ninguno");
  try {
    var caso = {
      doc_sistema: "IF_AE_REGULAR",
      doc_proceso: "EMISION REGULAR DE LICENCIAS DE FUNCIONAMIENTO",
      doc_id: casoid,
      // casoid
      doc_ci_nodo: nodo,
      // nodonombre
      doc_datos: "",
      doc_titulo: "",
      doc_palabras: "",
      doc_url: url_licencia,
      // 1) con superficie => nueva licencia //2) sin superficie => urllicencia
      doc_version: 1,
      doc_tiempo: 60,
      doc_firma_digital: "0",
      doc_estado: "",
      doc_usuario: "",
      doc_tipo_documento: "pdf",
      doc_tamanio_documento: "",
      doc_nombre: url_licencia,
      doc_tps_doc_id: 0,
      doc_url_logica: url_licencia,
      doc_acceso: "",
      doc_cuerpo: "",
      doc_tipo_documentacion: "",
      doc_tipo_ingreso: "",
      doc_estado_de_envio: "",
      doc_correlativo: "",
      doc_tipo_documento_ext: "",
      doc_nrotramite_nexo: "",
      doc_id_codigo: ae_nro_caso,
      // AE_NRO_CASO
      doc_id_carpeta: 0
    };
    return guardarLicenciaAe(token, caso).then(function (res) {
      log.estado = "finalizado";
      log.fin = new Date();
      log.param_in = {
        casoid: casoid,
        nodo: nodo,
        url_licencia: url_licencia,
        ae_nro_caso: ae_nro_caso
      };
      log.param_out = res.data;
      aelog.param_out.push(log);
      return res;
    }).catch(function (err) {
      log.estado = "fallido";
      log.error = err.toString();
      log.param_in = {
        casoid: casoid,
        nodo: nodo,
        url_licencia: url_licencia,
        ae_nro_caso: ae_nro_caso
      };
      log.fin = new Date();
      aelog.param_out.push(log);
      throw new Error(err).stack;
    });
  } catch (error) {
    log.estado = "fallido";
    log.error = error.toString();
    log.param_in = {
      casoid: casoid,
      nodo: nodo,
      url_licencia: url_licencia,
      ae_nro_caso: ae_nro_caso
    };
    log.fin = new Date();
    aelog.param_out.push(log);
    throw new Error(error).stack;
  }
}

function runGuardarFormCasos(token, full_data_lic) {
  var log = new Log(11, "Actualizar datos actividad economica CP", "Actualiza datos de AE que se encentran en cero papel.", "inciado", new Date(), undefined, "ninguno", "ninguno", "ninguno");
  try {
    var d = full_data_lic.data.success.data[0].datos;
    d.pago_adelantado = "PAGO ADELANTADO ANULADO";
    d.nodonombre = full_data_lic.data.success.data[0].nodonombre;
    d.nodoid = full_data_lic.data.success.data[0].nodoid;
    d.casoid = full_data_lic.data.success.data[0].casoid;
    d.ejecutado_por = "Sistema Sierra - Demonio anulación licencia, tipo pago adelantado."; // Parametro para identificar el sistema que ejecuto el proceso de anulacion de la licencia.

    var datosSerializados = JSON.stringify(d); // TODO: [doit] Obtener fecha actual e id usuario.

    var md = new Date();
    var datos_form = {
      datos: datosSerializados,
      fmodificado: "".concat(md.getFullYear(), "-").concat(md.getMonth() + 1, "-").concat(md.getDate()),
      usrid: "677",// TODO: Obtener el ID usuario.
      casid: full_data_lic.data.success.data[0].casoid // casoid
    };
    return guardarFormCasos(token, datos_form).then(function (res) {
      log.estado = "finalizado";
      log.fin = new Date();
      log.param_in = "Información completa de la actividad economica almacenada en cero papel.";
      log.param_out = res.data;
      aelog.param_out.push(log);
      return res;
    }).catch(function (err) {
      log.estado = "fallido";
      log.error = err.toString();
      log.param_in = "Información completa de la actividad economica almacenada en cero papel.";
      log.fin = new Date();
      aelog.param_out.push(log);
      throw new Error(err).stack;
    });
  } catch (error) {
    log.estado = "fallido";
    log.error = error.toString();
    log.param_in = "";
    log.fin = new Date();
    aelog.param_out.push(log);
    throw new Error(error).stack;
  }
}

function getSumarDias(fecha, dias) {
  try {
    /*var expresion = [/^((3[0-1])|(2[0-9])|(1[0-9])|(0[0-9]))([/])((1[0-2])|(0[1-9]))([/])(\d{4})$/gi, /^(\d{4})([/])((1[0-2])|(0[1-9]))([/])((3[0-1])|(2[0-9])|(1[0-9])|(0[0-9]))$/gi, /^((3[0-1])|(2[0-9])|(1[0-9])|(0[0-9]))([-])((1[0-2])|(0[1-9]))([-])(\d{4})$/gi, /^(\d{4})([-])((1[0-2])|(0[1-9]))([-])((3[0-1])|(2[0-9])|(1[0-9])|(0[0-9]))$/gi];
    var to = "";
    var new_f = "";
    if (fecha.match(expresion[0])) {
      new_f = fecha.split("/");
      new_f = new_f[2] + "-" + new_f[1] + "-" + new_f[0];
      to = new Date(new_f);
      to.setDate(to.getDate() + dias);
    } else if (fecha.match(expresion[1])) {
      new_f = fecha.split("/");
      new_f = new_f[0] + "-" + new_f[1] + "-" + new_f[2];
      to = new Date(new_f);
      to.setDate(to.getDate() + dias);
    } else if (fecha.match(expresion[2])) {
      new_f = fecha.split("-");
      new_f = new_f[2] + "-" + new_f[1] + "-" + new_f[0];
      to = new Date(new_f);
      to.setDate(to.getDate() + dias);
    } else if (fecha.match(expresion[3])) {
      to = new Date(fecha);
      to.setDate(to.getDate() + dias);
    } else {
      throw new Error("Fecha incorrecta.");
    }*/
    var aux = fecha.split('-');
    var fecha = new Date(aux[0], aux[1], aux[2]);
    fecha.setFullYear(fecha.getFullYear()+2);
    var m = (fecha.getMonth())+'';
    if(m.length == 1){
      m = '0'+(fecha.getMonth());
    }
    var res = fecha.getDate()+'/'+m+'/'+ fecha.getFullYear();
    var tf = [(fecha.getFullYear()+'-'+m+'-'+fecha.getDate()), res];
    return tf;
  } catch (error) {
    throw new Error(error).stack;
  }
}

module.exports = {
  anularLicencia: anularLicencia
};
