'use strict';

var _require = require('../../config/server-point');
var SIERRA_VALLE_SERVICIOS = _require.SIERRA_VALLE_SERVICIOS;
var SIERRA_VALLE_SERVICIO_LICENCIA = _require.SIERRA_VALLE_SERVICIO_LICENCIA;
var _require2 = require('../libs/http');
var postRequest = _require2.postRequest;
var VALLE_MOTOR = "/api/reglaNegocio/ejecutarWeb";

function obtenerAePagoAdelantado(token, ae_id) {
    try {
        var params = JSON.stringify({
            identificador: 'ACTIVIDAD_ECONOMICA-1994',
            parametros: '{"xae_id":'+ae_id+ '}'
        });
        var headers = getHeaders('json', token, params);
        var url = SIERRA_VALLE_SERVICIOS.split("|");
        var op = {
            host: url[0],
            port: url[1] || '',
            path: VALLE_MOTOR,
            headers: headers ? headers : null
        };

        return postRequest('valle.obtenerAePagoAdelantado', op, params).catch(function (err) {
            throw new Error(err).stack;
        });
    } catch (error) {
        throw new Error(error).stack;
    }
}

function anulaLicencia(token, id_fums, actividad_id, superficie) {
    try {
        var x = JSON.stringify(JSON.stringify(id_fums));
        var params = JSON.stringify({
            identificador: 'ACTIVIDAD_ECONOMICA-1996',
            //parametros: '{"xae_id":"'+actividad_id+'","xsuperficie":"'+superficie+'","odms":['+id_fums+']}'
            parametros: '{"xae_id":"'+actividad_id+'","xsuperficie":"'+superficie+'","odms":'+x+'}'
        });
        var headers = getHeaders('json', token, params);
        var url = SIERRA_VALLE_SERVICIOS.split("|");
        var op = {
            host: url[0],
            port: url[1] || '',
            path: VALLE_MOTOR,
            headers: headers ? headers : null
        };
        return postRequest('valle.anulaLicencia', op, params).catch(function (err) {
            throw new Error(err).stack;
        });
    } catch (error) {
        throw new Error(error).stack;
    }
}

function generarLicencia(token, formdata) {
    try {
        var headers = getHeaders('json', token, JSON.stringify(formdata));
        var url = SIERRA_VALLE_SERVICIO_LICENCIA.split("|");

        var op = {
            host: url[0],
            port: url[1] || '',
            path: '/api/licencia',//TODO path: '/api/licensesPago'
            headers: headers ? headers : null
        };
        return postRequest('valle.generarLicencia', op, JSON.stringify(formdata)).catch(function (err) {
            throw new Error(err).stack;
        });
    } catch (error) {
        throw new Error(error).stack;
    }
}

function guardarLicencia(token, licencia_datos) {
    try {
        var params = JSON.stringify({
            identificador: 'ACTIVIDAD_ECONOMICA-1999',
            parametros: JSON.stringify({ "xdatos_licencia": JSON.stringify(licencia_datos) })
        });

        var headers = getHeaders('json', token, params);

        var url = SIERRA_VALLE_SERVICIOS.split("|");

        var op = {
            host: url[0],
            port: url[1] || '',
            path: VALLE_MOTOR,
            headers: headers ? headers : null
        };

        return postRequest('valle.guardarLicencia', op, params).catch(function (err) {
            throw new Error(err).stack;
        });
    } catch (error) {
        throw new Error(error).stack;
    }
}

function guardarLog(token, log_data) {
    try {
        var params = JSON.stringify({
            identificador: 'ACTIVIDAD_ECONOMICA-2000',
            parametros: JSON.stringify(
                {
                    xdata: JSON.stringify(log_data),
                    xusr_id: 1,
                    xusr_ip: '127.0.0.1'
                }
            )
        });
        var headers = getHeaders('json', token, params);
        var url = SIERRA_VALLE_SERVICIOS.split("|");
        var op = {
            host: url[0],
            port: url[1] || '',
            path: VALLE_MOTOR,
            headers: headers ? headers : null
        };
        return postRequest('valle.guardarLog', op, params).catch(function (err) {
            throw new Error(err).stack;
        });
    } catch (error) {
        throw new Error(error).stack;
    }
}

function getHeaders(tipo, token, params) {
    var headers = "";
    try {
        if (tipo == 'xml') {
            headers = {
                'authorization': 'Bearer ' + token,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(params)
            };
        } else {
            headers = {
                'authorization': 'Bearer ' + token,
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(params)
            };
        }

        return headers;
    } catch (error) {
        throw new Error(error).stack;
    }
}

module.exports = {
    obtenerAePagoAdelantado: obtenerAePagoAdelantado,
    anulaLicencia: anulaLicencia,
    generarLicencia: generarLicencia,
    guardarLicencia: guardarLicencia,
    guardarLog: guardarLog
};