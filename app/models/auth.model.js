'use strict';

var _require = require('../../config/server-point');

var SIERRA_VALLE_SERVICIOS = _require.SIERRA_VALLE_SERVICIOS;

var acc = require('../../config/auth-config')[process.env.NODE_ENV || 'development'];

var _require2 = require('../libs/http');

var postRequest = _require2.postRequest;

var SIERRA_MOTOR = "/api/apiLogin";

function getTokenMotor() {
    try {
        var params = JSON.stringify({
            "usr_usuario": acc.user,
            "usr_clave": acc.password
        });

        var url = SIERRA_VALLE_SERVICIOS.split("|");

        var op = {
            host: url[0],
            port: url[1] || '',
            path: SIERRA_MOTOR,
            headers: null
        };

        return postRequest('auth.getTokenMotor', op, params).catch(function (err) {
            throw new Error(err).stack;
        });
    } catch (error) {
        throw new Error(error).stack;
    }
}

function getUsuario() {
    return acc.user;
}

module.exports = {
    getTokenMotor: getTokenMotor,
    getUsuario: getUsuario
};