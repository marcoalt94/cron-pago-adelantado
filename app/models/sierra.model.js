'use strict';

var _require = require('../../config/server-point');

var SIERRA_VALLE_SERVICIOS = _require.SIERRA_VALLE_SERVICIOS;

var _require2 = require('../libs/http');

var postRequest = _require2.postRequest;

var SIERRA_MOTOR = "/api/reglaNegocio/ejecutarWeb";

function listarFumPagoAdelantado(token) {
    try {
        var tk = 'Bearer ' + token;
        var params = JSON.stringify({
            identificador: 'ACTIVIDAD_ECONOMICA-1993',
            parametros: "{}"
        });

        var headers = {
            'authorization': tk,
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(params)
        };

        var url = SIERRA_VALLE_SERVICIOS.split("|");

        var op = {
            host: url[0],
            port: url[1] || '',
            path: SIERRA_MOTOR,
            headers: headers ? headers : null
        };

        return postRequest('sierra.listarFumPagoAdelantado', op, params).catch(function (err) {
            throw new Error(err).stack;
        });
    } catch (error) {
        throw new Error(error).stack;
    }
}

module.exports = {
    listarFumPagoAdelantado: listarFumPagoAdelantado
};