'use strict';

var _require = require('../../config/server-point');

var IGOB_NOTIFICACION = _require.IGOB_NOTIFICACION;
var IGOB_GUARDAR_LIC = _require.IGOB_GUARDAR_LIC;
var IGOB_LICENCIA = _require.IGOB_LICENCIA;

var _require2 = require('../libs/http');

var postRequest = _require2.postRequest;

function getAeDatos(token, params) {
    try {
        var url = IGOB_LICENCIA.split("|");
        var headers = getHeaders('json', token, JSON.stringify(params));

        var op = {
            host: url[0],
            port: url[1] || '',
            path: '/wsIf/lstTramitesPagoAdelantado',
            headers: headers ? headers : null
        };

        return postRequest('igob.getAeDatos', op, JSON.stringify(params)).catch(function (err) {
            throw new Error(err).stack;
        });
    } catch (error) {
        throw new Error(error).stack;
    }
}

function igobNotificacion(token, notificacion) {
    try {
        var url = IGOB_NOTIFICACION.split("|");
        var headers = getHeaders('json', token, JSON.stringify(notificacion));

        var op = {
            host: url[0],
            port: url[1] || '',
            path: '/wsRCPG/crearnotificacion',
            headers: headers ? headers : null
        };

        return postRequest('igob.igobNotificacion', op, JSON.stringify(notificacion)).catch(function (err) {
            throw new Error(err).stack;
        });
    } catch (error) {
        throw new Error(error).stack;
    }
}

function igobGuardarLic(token, doc_licencia) {
    try {
        var url = IGOB_GUARDAR_LIC.split("|");
        var headers = getHeaders('json', token, JSON.stringify(doc_licencia));

        var op = {
            host: url[0],
            port: url[1] || '',
            path: '/wsRCPG/gDocumentosIf',
            headers: headers ? headers : null
        };

        return postRequest('igob.igobGuardarLic', op, JSON.stringify(doc_licencia)).catch(function (err) {
            throw new Error(err).stack;
        });
    } catch (error) {
        throw new Error(error).stack;
    }
}

function guardarLicenciaAe(token, caso) { //guardar url en cero papel
    try {
        var url = IGOB_LICENCIA.split("|");
        var headers = getHeaders('json', token, JSON.stringify(caso));

        var op = {
            host: url[0],
            port: url[1] || '',
            path: '/wsIf/crearAdjunto',
            headers: headers ? headers : null
        };

        return postRequest('igob.guardarLicenciaAe', op, JSON.stringify(caso)).catch(function (err) {
            throw new Error(err).stack;
        });
    } catch (error) {
        throw new Error(err).stack;
    }
}

function guardarFormCasos(token, full_data_lic) {
    try {
        var url = IGOB_LICENCIA.split("|");
        var headers = getHeaders('json', token, JSON.stringify(full_data_lic));

        var op = {
            host: url[0],
            port: url[1] || '',
            path: '/wsIf/guardarFormCasos',
            headers: headers ? headers : null
        };

        return postRequest('igob.guardarFormCasos', op, JSON.stringify(full_data_lic)).catch(function (err) {
            throw new Error(err).stack;
        });
    } catch (error) {
        throw new Error(error).stack;
    }
}

function getHeaders(tipo, token, params) {
    var headers = "";
    try {
        if (tipo == 'xml') {
            headers = {
                'authorization': 'Bearer ' + token,
                'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
                'Content-Length': Buffer.byteLength(params)
            };
        } else {
            headers = {
                'authorization': 'Bearer ' + token,
                'Content-Type': 'application/json; charset=utf-8',
                'Content-Length': Buffer.byteLength(params)
            };
        }

        return headers;
    } catch (error) {
        throw new Error(error).stack;
    }
}

module.exports = {
    igobNotificacion: igobNotificacion,
    igobGuardarLic: igobGuardarLic,
    guardarLicenciaAe: guardarLicenciaAe,
    guardarFormCasos: guardarFormCasos,
    getAeDatos: getAeDatos
};